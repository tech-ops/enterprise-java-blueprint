# Training Plan
* Introduction
    * Technology overview
    * Project Guidelines
    * Application Layers and Components
    * Choose business use case (Pet Shop; Trips; Purchase Orders; ...)

* Requirement analysis
* Architecture design

* Setup
    * Install software
    * Build tool and Application Framework (Maven, Java, Spring Initializr)
    * Static analysis

* Software construction  
    * Model database (JPA and use a tool for database change management)
    * Implement Web API (REST with a tool for documentation) and Services (include logging)
    * Implement tests (unit, integration with JUnit and Mockito)

* Demo
* Discussion
    * Used technologies
    * Other/Alternative technologies



