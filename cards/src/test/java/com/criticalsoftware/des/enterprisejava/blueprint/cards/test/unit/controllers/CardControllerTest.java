package com.criticalsoftware.des.enterprisejava.blueprint.cards.test.unit.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CreateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.UpdateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.services.CardIssuerService;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.services.CardService;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.test.fixtures.CardTestFixtures;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CardControllerTest {

    @MockBean
    private CardIssuerService cardIssuerService;

    @MockBean
    private CardService cardService;

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    /**
     * Test for verifying that the controller endpoints are working properly.
     * <p>
     * Scenario: Create a new Card;
     * <p>
     * Expected result: Endpoint should return 201;
     */
    @Test
    public void createCard() throws Exception {
        //Setup
        CreateCardDto request = CardTestFixtures.getCreateCardDto();

        //Test
        mockMvc.perform(post("/api/cards")
            .content(mapper.writeValueAsString(request))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
    }

    /**
     * Test for verifying that the controller endpoints are working properly.
     * <p>
     * Scenario: Retrieve a Card;
     * <p>
     * Expected result: Endpoint should return 200;
     */
    @Test
    public void retrieveCard() throws Exception {
        //Setup
        CardDto response = CardTestFixtures.getCardResponseDto();
        Mockito.when(cardService.getCardById(Mockito.anyLong())).thenReturn(response);

        //Test
        mockMvc.perform(get("/api/cards/1"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.cardNumber").value(response.getCardNumber()))
            .andExpect(jsonPath("$.firstName").value(response.getFirstName()))
            .andExpect(jsonPath("$.lastName").value(response.getLastName()))
            .andExpect(jsonPath("$.address").value(response.getAddress()))
            .andExpect(jsonPath("$.phoneNumber").value(response.getPhoneNumber()))
            .andExpect(jsonPath("$.expireDate").value(response.getExpireDate()))
            .andExpect(jsonPath("$.cardIssuer.id").value(response.getCardIssuer().getId()))
            .andExpect(jsonPath("$.cardIssuer.name").value(response.getCardIssuer().getName()))
            .andExpect(jsonPath("$.cardIssuer.description").value(response.getCardIssuer().getDescription()));

    }

    /**
     * Test for verifying that the controller endpoints are working properly.
     * <p>
     * Scenario: Edit a Card;
     * <p>
     * Expected result: Endpoint should return 200.
     */
    @Test
    public void editCard() throws Exception {
        //Setup
        UpdateCardDto request = CardTestFixtures.getEditCardDto();

        //Test
        mockMvc.perform(put("/api/cards/1")
            .content(mapper.writeValueAsString(request))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());

    }

    /**
     * Test for verifying that the controller endpoints are working properly.
     * <p>
     * Scenario: Delete a Card;
     * <p>
     * Expected result: Endpoint should return 200;
     */
    @Test
    public void deleteCard() throws Exception {
        //Test
        mockMvc.perform(delete("/api/cards/1"))
            .andExpect(status().isOk());

    }
}