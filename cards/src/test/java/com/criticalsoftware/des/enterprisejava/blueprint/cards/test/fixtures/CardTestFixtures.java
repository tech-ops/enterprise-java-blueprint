package com.criticalsoftware.des.enterprisejava.blueprint.cards.test.fixtures;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CreateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.UpdateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.Card;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;

public class CardTestFixtures {

    public static CardIssuer getCardIssuerEntity() {
        CardIssuer cardIssuerEntity = new CardIssuer();
        cardIssuerEntity.setId(1L);
        cardIssuerEntity.setName("VISA");
        return cardIssuerEntity;
    }

    public static CardIssuerDto getCardIssuerDto() {
        return new CardIssuerDto(1L, "VISA", null);
    }

    public static Card getCardEntity() {
        Card cardEntity = new Card();
        cardEntity.setId(1L);
        cardEntity.setCardNumber(123456);
        cardEntity.setFirstName("Critical");
        cardEntity.setLastName("Software");
        cardEntity.setAddress("Largo do Dr. Tito Fontes 21, 4000-124 Porto");
        cardEntity.setPhoneNumber("229431148");
        cardEntity.setExpireDate(1578492288L);
        cardEntity.setCvv("180");
        cardEntity.setCardIssuer(getCardIssuerEntity());

        return cardEntity;
    }

    public static CardDto getCardResponseDto() {
        CardDto cardDto = new CardDto();
        cardDto.setCardNumber(78910);
        cardDto.setFirstName("Critical");
        cardDto.setLastName("Software");
        cardDto.setAddress("Largo do Dr. Tito Fontes 21, 4000-124 Porto");
        cardDto.setPhoneNumber("229431148");
        cardDto.setExpireDate(1578492288L);
        cardDto.setCardIssuer(getCardIssuerDto());

        return cardDto;
    }

    public static CreateCardDto getCreateCardDto() {
        CreateCardDto createCardDto = new CreateCardDto();
        createCardDto.setCardNumber(78910);
        createCardDto.setFirstName("Critical");
        createCardDto.setLastName("Software");
        createCardDto.setAddress("Largo do Dr. Tito Fontes 21, 4000-124 Porto");
        createCardDto.setPhoneNumber("229431148");
        createCardDto.setCvv("123");
        createCardDto.setExpireDate(1578492288L);
        createCardDto.setCardIssuerId(1L);

        return createCardDto;
    }

    public static UpdateCardDto getEditCardDto() {
        UpdateCardDto updateCardDto = new UpdateCardDto();
        updateCardDto.setLastName("Software");
        updateCardDto.setAddress("Largo do Dr. Tito Fontes 21, 4000-124 Porto");
        updateCardDto.setPhoneNumber("229431148");

        return updateCardDto;
    }

}
