package com.criticalsoftware.des.enterprisejava.blueprint.cards.test.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CreateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.Card;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.repositories.CardRepository;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.test.fixtures.CardTestFixtures;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class CardIntegrationTest {

    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private MockMvc mockMvc;

    /**
     * Integration test for verifying if a card is correctly created.
     * <p>
     * Scenario: Create a new Card;
     * <p>
     */
    @Test
    public void shouldCreateCard() throws Exception {
        //Setup
        CreateCardDto expectedCard = CardTestFixtures.getCreateCardDto();

        Assert.assertEquals(0, cardRepository.count());

        //Test POST
        mockMvc.perform(post("/api/cards/")
            .content(mapper.writeValueAsString(expectedCard))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());

        //Test DB
        Assert.assertEquals(1, cardRepository.count());

        final Card createdCard = cardRepository.findById(1L).get();

        Assert.assertEquals(expectedCard.getCardNumber(), createdCard.getCardNumber());
        Assert.assertEquals(expectedCard.getFirstName(), createdCard.getFirstName());
        Assert.assertEquals(expectedCard.getLastName(), createdCard.getLastName());
        Assert.assertEquals(expectedCard.getAddress(), createdCard.getAddress());
        Assert.assertEquals(expectedCard.getPhoneNumber(), createdCard.getPhoneNumber());
        Assert.assertEquals(expectedCard.getExpireDate(), createdCard.getExpireDate());
    }
}

