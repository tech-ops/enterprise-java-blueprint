package com.criticalsoftware.des.enterprisejava.blueprint.cards.test.unit.services;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.Card;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions.CardNotFoundException;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.repositories.CardRepository;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.services.CardService;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.test.fixtures.CardTestFixtures;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CardServiceTest {

    @Mock
    CardRepository cardRepository;

    @InjectMocks
    private CardService cardService;

    @Test
    public void getCardById() throws CardNotFoundException {
        //Setup
        Card expectedResult = CardTestFixtures.getCardEntity();
        Optional<Card> optionalCard = Optional.of(expectedResult);
        Mockito.when(cardRepository.findById(Mockito.anyLong())).thenReturn(optionalCard);

        final CardDto actualResult = cardService.getCardById(expectedResult.getId());

        //Test
        Assert.assertEquals(expectedResult.getCardNumber(), actualResult.getCardNumber());
        Assert.assertEquals(expectedResult.getFirstName(), actualResult.getFirstName());
        Assert.assertEquals(expectedResult.getLastName(), actualResult.getLastName());
        Assert.assertEquals(expectedResult.getAddress(), actualResult.getAddress());
        Assert.assertEquals(expectedResult.getPhoneNumber(), actualResult.getPhoneNumber());
        Assert.assertEquals(expectedResult.getExpireDate(), actualResult.getExpireDate());
    }

    @Test(expected = CardNotFoundException.class)
    public void shouldNotFindCard() throws CardNotFoundException {
        // Setup
        Card card = CardTestFixtures.getCardEntity();
        // Sets an invalid id.
        card.setId(2345L);
        Mockito.when(cardRepository.getOne(Mockito.anyLong())).thenReturn(card);

        cardService.getCardById(card.getId());

    }
}