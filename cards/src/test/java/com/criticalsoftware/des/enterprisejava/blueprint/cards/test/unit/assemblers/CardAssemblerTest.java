package com.criticalsoftware.des.enterprisejava.blueprint.cards.test.unit.assemblers;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.assemblers.CardAssembler;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CreateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.Card;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.test.fixtures.CardTestFixtures;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class CardAssemblerTest {

    @Test
    public void convertsEntityToDto() {
        //Setup
        final Card actualResult = CardTestFixtures.getCardEntity();
        final CardDto expectedResult = CardAssembler.toDto(actualResult);

        //Test
        Assert.assertEquals(expectedResult.getCardNumber(), actualResult.getCardNumber());
        Assert.assertEquals(expectedResult.getFirstName(), actualResult.getFirstName());
        Assert.assertEquals(expectedResult.getLastName(), actualResult.getLastName());
        Assert.assertEquals(expectedResult.getAddress(), actualResult.getAddress());
        Assert.assertEquals(expectedResult.getPhoneNumber(), actualResult.getPhoneNumber());
        Assert.assertEquals(expectedResult.getExpireDate(), actualResult.getExpireDate());
    }

    @Test
    public void convertsDtoToEntity() {
        //Setup
        final CreateCardDto expectedResult = CardTestFixtures.getCreateCardDto();
        final Card actualResult = CardAssembler.fromDto(expectedResult);

        //Test
        Assert.assertEquals(expectedResult.getCardNumber(), actualResult.getCardNumber());
        Assert.assertEquals(expectedResult.getFirstName(), actualResult.getFirstName());
        Assert.assertEquals(expectedResult.getLastName(), actualResult.getLastName());
        Assert.assertEquals(expectedResult.getAddress(), actualResult.getAddress());
        Assert.assertEquals(expectedResult.getPhoneNumber(), actualResult.getPhoneNumber());
        Assert.assertEquals(expectedResult.getExpireDate(), actualResult.getExpireDate());
        Assert.assertEquals(expectedResult.getCvv(), actualResult.getCvv());

    }
}
