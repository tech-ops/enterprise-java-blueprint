package com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions;

import com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.AbstractCustomException;

public class CardIssuerNotFoundException extends AbstractCustomException {

    public CardIssuerNotFoundException(Long id) {
        super(CardErrorCode.CARD_ISSUER_NOT_FOUND, id);
    }

}
