package com.criticalsoftware.des.enterprisejava.blueprint.cards.controllers;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.UpdateCardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.services.CardIssuerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cardissuers")
public class CardIssuerController {

    private CardIssuerService cardIssuerService;

    public CardIssuerController(final CardIssuerService cardIssuerService) {
        this.cardIssuerService = cardIssuerService;
    }

    /**
     * Retrieves a list of CardIssuers {@link CardIssuerDto}.
     *
     * @return an instance of {@link ResponseEntity} with result status and the list of {@link CardIssuerDto}.
     */
    @GetMapping("/")
    @Operation(summary = "Gets the list of all Card Issuers",
        responses = {
            @ApiResponse(responseCode = "200", description = "The list of Card Issuers",
                content = @Content(mediaType = "application/json"))})
    public ResponseEntity<List<CardIssuerDto>> getAllCardIssuers() {

        List<CardIssuerDto> cardIssuers = cardIssuerService.getAllCardIssuers();

        return new ResponseEntity<>(cardIssuers, HttpStatus.OK);
    }

    /**
     * Updates an instance of CardIssuer {@link CardIssuer} given its identifier.
     *
     * @param editCardIssuer an instance of {@link UpdateCardIssuerDto}.
     * @param id             the card identifier.
     * @return an instance of {@link ResponseEntity} with result status.
     */
    @PutMapping("/{id}")
    @Operation(summary = "Update an existing Card Issuer",
        responses = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Card Issuer not found",
                content = @Content(schema = @Schema(implementation = Void.class)))})
    public ResponseEntity<Void> updateCardIssuer(@PathVariable final Long id,
                                                 @RequestBody final UpdateCardIssuerDto editCardIssuer) {

        cardIssuerService.updateCardIssuer(id, editCardIssuer);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
