package com.criticalsoftware.des.enterprisejava.blueprint.cards.assemblers;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CreateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.Card;

public class CardAssembler {

    /**
     * Transforms an instance of CreateCardDto {@link CreateCardDto} to an instance of Card {@link Card}.
     *
     * @param dto an instance of {@link CreateCardDto}.
     * @return the {@link Card} entity
     */
    public static Card fromDto(final CreateCardDto dto) {
        Card card = new Card();
        card.setCardNumber(dto.getCardNumber());
        card.setFirstName(dto.getFirstName());
        card.setLastName(dto.getLastName());
        card.setAddress(dto.getAddress());
        card.setPhoneNumber(dto.getPhoneNumber());
        card.setExpireDate(dto.getExpireDate());
        card.setCvv(dto.getCvv());
        return card;
    }

    /**
     * Transforms an instance of Card {@link Card} to an instance of CardResponseDto {@link CardDto}.
     *
     * @param card an instance of {@link Card}.
     * @return the {@link CardDto}
     */
    public static CardDto toDto(final Card card) {
        CardDto dto = new CardDto();
        dto.setCardNumber(card.getCardNumber());
        dto.setFirstName(card.getFirstName());
        dto.setLastName(card.getLastName());
        dto.setAddress(card.getAddress());
        dto.setPhoneNumber(card.getPhoneNumber());
        dto.setExpireDate(card.getExpireDate());
        if (card.getCardIssuer() != null) {
            dto.setCardIssuer(CardIssuerAssembler.toDto(card.getCardIssuer()));
        }
        return dto;
    }
}
