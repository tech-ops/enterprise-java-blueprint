package com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos;

public class UpdateCardIssuerDto {

    private String description;

    public UpdateCardIssuerDto() {
    }

    public UpdateCardIssuerDto(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
