package com.criticalsoftware.des.enterprisejava.blueprint.cards.repositories;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardIssuerRepository extends JpaRepository<CardIssuer, Long> {

}
