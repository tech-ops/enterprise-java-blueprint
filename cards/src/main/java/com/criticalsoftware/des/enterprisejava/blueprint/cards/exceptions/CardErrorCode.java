package com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions;

public enum CardErrorCode implements CardResourceCode {

    INVALID_INPUT_PARAMETERS("card.invalid_input_parameters", "Invalid input parameters."),

    CARD_NOT_FOUND("card.card_not_found", "Card not found for id: {0}"),

    CARD_ISSUER_NOT_FOUND("card.card_issuer_not_found", "Card Issuer not found for id: {0}");

    private String resourceCode;
    private String defaultErrorMessage;

    CardErrorCode(String resourceCode, String defaultErrorMessage) {
        this.resourceCode = resourceCode;
        this.defaultErrorMessage = defaultErrorMessage;
    }

    @Override
    public String getDefaultErrorMessage() {
        return this.defaultErrorMessage;
    }

    @Override
    public String getResourceCode() {
        return this.resourceCode;
    }
}
