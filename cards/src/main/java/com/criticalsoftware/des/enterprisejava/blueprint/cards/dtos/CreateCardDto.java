package com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateCardDto {

    @NotNull(message = "Please provide a card number")
    private Integer cardNumber;
    @NotBlank(message = "Please provide a first name")
    @Size(max = 16)
    private String firstName;
    @NotBlank(message = "Please provide a last name")
    @Size(max = 16)
    private String lastName;
    @Size(max = 255)
    private String address;
    @Size(max = 64)
    private String phoneNumber;
    @NotNull(message = "Please provide an expire date")
    private Long expireDate;
    @NotBlank(message = "Please provide a CVV")
    @Size(min = 3, max = 3, message = "CVV must have 3 numbers")
    private String cvv;
    @NotNull(message = "Please provide a card issuer")
    private Long cardIssuerId;

    public CreateCardDto() {
    }

    public Integer getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(final Integer cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(final Long expireDate) {
        this.expireDate = expireDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(final String cvv) {
        this.cvv = cvv;
    }

    public Long getCardIssuerId() {
        return cardIssuerId;
    }

    public void setCardIssuerId(final Long cardIssuerId) {
        this.cardIssuerId = cardIssuerId;
    }
}
