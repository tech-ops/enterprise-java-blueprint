package com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.handlers;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions.CardErrorCode;
import com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.AbstractCustomException;
import com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.ResultErrorDto;
import com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.ValidationError;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestExceptionHandler.class);

    /**
     * Handles all generic exceptions and translates to internal server error.
     *
     * @param ex      the exception to handle
     * @param request the web request
     * @return the {@link ResponseEntity} that holds the error code and additional information
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity handleAllExceptions(Exception ex, WebRequest request) {
        LOG.error("Unexpected Error caused by: ", ex);

        ResultErrorDto error = new ResultErrorDto();
        return new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles all application custom exceptions and translates to bas request.
     *
     * @param ex      the exception to handle
     * @param request the web request
     * @return the {@link ResponseEntity} that holds the error code and additional information
     */
    @ExceptionHandler(AbstractCustomException.class)
    public final ResponseEntity handleAbstractCustomException(AbstractCustomException ex, WebRequest request) {
        LOG.error("Error processing request, caused by: ", ex);

        ResultErrorDto error = new ResultErrorDto();
        error.setErrorCode(ex.getErrorCode().getResourceCode());
        error.setMessage(ex.getMessage());
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }


    @Override
    public ResponseEntity handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers,
                                                       HttpStatus status, WebRequest request) {
        LOG.error("Validation error caused by: ", exception);

        ResultErrorDto resultErrorDto = new ResultErrorDto();
        resultErrorDto.setErrorCode(CardErrorCode.INVALID_INPUT_PARAMETERS.getResourceCode());
        resultErrorDto.setMessage(CardErrorCode.INVALID_INPUT_PARAMETERS.getDefaultErrorMessage());

        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        for (FieldError fieldError : fieldErrors) {
            ValidationError validationError = new ValidationError();
            validationError.setObject(fieldError.getObjectName());
            validationError.setField(fieldError.getField());
            validationError.setMessage(fieldError.getDefaultMessage());
            validationError.setErrorCode(fieldError.getCode());
            resultErrorDto.addSubError(validationError);
        }

        return new ResponseEntity<>(resultErrorDto, HttpStatus.BAD_REQUEST);
    }

}
