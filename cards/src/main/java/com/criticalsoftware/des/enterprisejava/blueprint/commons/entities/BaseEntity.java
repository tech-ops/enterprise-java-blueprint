package com.criticalsoftware.des.enterprisejava.blueprint.commons.entities;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

@MappedSuperclass
public class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Version
    @Column(nullable = false)
    private Long version;

    @Column(name = "audit_create_datetime", nullable = false)
    private Long auditCreateDatetime;

    @Column(name = "audit_change_datetime", nullable = false)
    private Long auditChangeDateTime;

    public BaseEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuditCreateDatetime() {
        return auditCreateDatetime;
    }

    protected void setAuditCreateDatetime(Long auditCreateDatetime) {
        this.auditCreateDatetime = auditCreateDatetime;
    }

    public Long getAuditChangeDateTime() {
        return auditChangeDateTime;
    }

    protected void setAuditChangeDateTime(Long chnageDateTime) {
        this.auditChangeDateTime = chnageDateTime;
    }

    @PrePersist
    private void updateCreateDateTime() {
        this.auditCreateDatetime = Instant.now().toEpochMilli();
        this.auditChangeDateTime = Instant.now().toEpochMilli();
    }

    @PreUpdate
    private void updateChangeDateTime() {
        this.auditChangeDateTime = Instant.now().toEpochMilli();
    }
}
