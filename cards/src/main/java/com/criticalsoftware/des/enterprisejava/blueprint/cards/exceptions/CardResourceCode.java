package com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions;

import com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.ResourceCode;

public interface CardResourceCode extends ResourceCode {

    String getDefaultErrorMessage();

}
