package com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos;

public class CardIssuerDto {

    private Long id;
    private String name;
    private String description;

    public CardIssuerDto() {
    }

    /**
     * Creates a new instance of {@link CardIssuerDto}.
     *
     * @param id          the card issuer identifier.
     * @param name        the card issuers name.
     * @param description the card issuers description.
     */
    public CardIssuerDto(final Long id, final String name, final String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
