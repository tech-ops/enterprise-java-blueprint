package com.criticalsoftware.des.enterprisejava.blueprint.cards.controllers;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CreateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.UpdateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.Card;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions.CardIssuerNotFoundException;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions.CardNotFoundException;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.services.CardService;
import com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.ResultErrorDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/cards")
public class CardController {

    private CardService cardService;

    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    /**
     * Creates an instance of Card {@link Card}.
     *
     * @param request an instance of {@link CreateCardDto}
     * @return an instance of  {@link ResponseEntity} to inform a Card was created
     * @throws CardIssuerNotFoundException if the card issuer is invalid
     */
    @PostMapping
    @Operation(summary = "Create new Card",
        responses = {
            @ApiResponse(responseCode = "201", content = @Content(mediaType = "application/json",
                schema = @Schema(implementation = Void.class))),
            @ApiResponse(responseCode = "400", description = "Invalid Card information",
                content = @Content(mediaType = "application/json",
                    schema = @Schema(implementation = ResultErrorDto.class))),
        })
    public ResponseEntity<Void> createCard(@Valid @RequestBody final CreateCardDto request)
        throws CardIssuerNotFoundException {

        cardService.createCard(request);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Retrieves an instance of CardResponseDto {@link CardDto}.
     *
     * @param id the card identifier.
     * @return the card information.
     */
    @GetMapping("/{id}")
    @Operation(summary = "Gets the Card information",
        responses = {
            @ApiResponse(responseCode = "200", description = "The Card information",
                content = @Content(mediaType = "application/json", schema = @Schema(implementation = CardDto.class))),
            @ApiResponse(responseCode = "400", description = "Invalid Card information",
                content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                    ResultErrorDto.class))),
            @ApiResponse(responseCode = "404", description = "Card not found",
                content = @Content(schema = @Schema(implementation = Void.class)))})
    public ResponseEntity<CardDto> getCardById(
        @Parameter(description = "The card identifier") @PathVariable final Long id) {

        CardDto response;
        try {
            response = cardService.getCardById(id);
        } catch (CardNotFoundException exception) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * Updates an instance of Card {@link Card} given its identifier.
     *
     * @param request an instance of {@link UpdateCardDto}.
     * @param id      the card identifier.
     * @return an instance of {@link ResponseEntity} to inform a Card was updated.
     */
    @PutMapping("/{id}")
    @Operation(summary = "Update an existing Card",
        responses = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "400", description = "Invalid Card information",
                content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                    ResultErrorDto.class))),
            @ApiResponse(responseCode = "404", description = "Card not found",
                content = @Content(schema = @Schema(implementation = Void.class)))})
    public ResponseEntity<CardDto> updateCard(
        @Parameter(description = "The Card identifier") @PathVariable final Long id,
        @RequestBody final UpdateCardDto request) {
        try {
            cardService.updateCard(id, request);
        } catch (CardNotFoundException exception) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Deletes an instance of Card {@link Card} given its identifier.
     *
     * @param id the card identifier.
     * @return an instance of {@link ResponseEntity} to inform a Card was deleted.
     */
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete an existing card",
        responses = {@ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Card not found",
                content = @Content(schema = @Schema(implementation = Void.class)))})
    public ResponseEntity<String> deleteCard(@Parameter(description = "The Card identifier")
                                             @PathVariable final Long id) {

        cardService.deleteCardById(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
