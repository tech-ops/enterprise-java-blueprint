package com.criticalsoftware.des.enterprisejava.blueprint.cards.entities;

import com.criticalsoftware.des.enterprisejava.blueprint.commons.entities.BaseEntity;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class CardIssuer extends BaseEntity {

    @Column(nullable = false)
    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
