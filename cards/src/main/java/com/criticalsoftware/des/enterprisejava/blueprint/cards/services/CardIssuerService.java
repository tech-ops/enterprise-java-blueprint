package com.criticalsoftware.des.enterprisejava.blueprint.cards.services;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.assemblers.CardIssuerAssembler;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.UpdateCardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.repositories.CardIssuerRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to provide all operations on Card Issuers.
 */
@Service
@CacheConfig(cacheNames = "card-issue")
public class CardIssuerService {

    private CardIssuerRepository cardIssuerRepository;

    /**
     * Creates a new instance of {@link CardIssuerService}.
     *
     * @param cardIssuerRepository the repository for the Card Issuer
     */
    public CardIssuerService(final CardIssuerRepository cardIssuerRepository) {
        this.cardIssuerRepository = cardIssuerRepository;
    }

    /**
     * Gets the card with a specific id.
     *
     * @param id the card issuer identifier
     * @return the list of {@link CardIssuerDto}
     */
    public CardIssuerDto getCardIssuerDtoById(Long id) {
        final CardIssuer cardIssuer = this.cardIssuerRepository.findById(id).get();
        return CardIssuerAssembler.toDto(cardIssuer);
    }

    /**
     * Gets the list of all Card Issuers.
     *
     * @return the list of {@link CardIssuerDto}
     */
    @Cacheable
    public List<CardIssuerDto> getAllCardIssuers() {
        return this.cardIssuerRepository.findAll().stream().map(cardIssuer -> CardIssuerAssembler.toDto(cardIssuer))
            .collect(Collectors.toList());
    }

    /**
     * Gets the {@link CardIssuer} by identifier.
     *
     * @param cardIssuerId the card issuer identifier
     * @return the {@link CardIssuer} entity
     */
    public Optional<CardIssuer> getCardIssuerById(final Long cardIssuerId) {
        return this.cardIssuerRepository.findById(cardIssuerId);
    }

    /**
     * Edit the {@link CardIssuer} entity with new information.
     *
     * @param id               the Card Issuer identifier
     * @param updateCardIssuer holds the information to update
     */
    @CacheEvict(allEntries = true)
    @Transactional
    public void updateCardIssuer(final Long id, final UpdateCardIssuerDto updateCardIssuer) {
        CardIssuer cardIssuer = this.cardIssuerRepository.getOne(id);
        cardIssuer.setDescription(updateCardIssuer.getDescription());
        this.cardIssuerRepository.save(cardIssuer);
    }
}
