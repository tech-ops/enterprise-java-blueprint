package com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos;

public class CardDto {

    private Integer cardNumber;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;
    private Long expireDate;
    private CardIssuerDto cardIssuer;

    public CardDto() {
    }

    /**
     * Creates a new instance of {@link CardDto}.
     *
     * @param cardNumber  the card number.
     * @param firstName   the card owner first name.
     * @param lastName    the card owner last name.
     * @param address     the card owner address.
     * @param phoneNumber the card owner phone number.
     * @param expireDate  the card expire date.
     * @param cardIssuer  the card issuer .
     */
    public CardDto(final Integer cardNumber, final String firstName, final String lastName,
                   final String address, final String phoneNumber, final Long expireDate,
                   final CardIssuerDto cardIssuer) {

        this.cardNumber = cardNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.expireDate = expireDate;
        this.cardIssuer = cardIssuer;
    }

    public Integer getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(final Integer cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(final Long expireDate) {
        this.expireDate = expireDate;
    }

    public CardIssuerDto getCardIssuer() {
        return cardIssuer;
    }

    public void setCardIssuer(final CardIssuerDto cardIssuer) {
        this.cardIssuer = cardIssuer;
    }
}
