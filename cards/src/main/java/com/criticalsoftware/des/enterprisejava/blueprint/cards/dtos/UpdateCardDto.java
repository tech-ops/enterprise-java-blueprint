package com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos;

public class UpdateCardDto {

    private String lastName;
    private String phoneNumber;
    private String address;

    public UpdateCardDto() {
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(final String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }
}
