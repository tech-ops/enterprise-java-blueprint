package com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions;

import com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions.AbstractCustomException;

public class CardNotFoundException extends AbstractCustomException {

    public CardNotFoundException(Long id) {
        super(CardErrorCode.CARD_NOT_FOUND, id);
    }
}
