package com.criticalsoftware.des.enterprisejava.blueprint.cards.services;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.assemblers.CardAssembler;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CreateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.UpdateCardDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.Card;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions.CardIssuerNotFoundException;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.exceptions.CardNotFoundException;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.repositories.CardRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service to provide all operations on Cards.
 */
@Service
public class CardService {

    private static final Logger LOG = LoggerFactory.getLogger(CardService.class);

    private final CardRepository cardRepository;

    private final CardIssuerService cardIssuerService;

    /**
     * Creates a new instance of {@link CardService}.
     *
     * @param cardRepository    the {@link CardRepository} for handling data
     * @param cardIssuerService the {@link CardIssuerService} for handling Card Issue operations
     */
    public CardService(final CardRepository cardRepository, final CardIssuerService cardIssuerService) {
        this.cardRepository = cardRepository;
        this.cardIssuerService = cardIssuerService;
    }

    /**
     * Creates a new Card with provided information on {@link CreateCardDto}.
     *
     * @param createCardDto holds information about the Card to create
     * @throws CardIssuerNotFoundException if the Card Issuer is invalid.
     */
    @Transactional
    public void createCard(CreateCardDto createCardDto) throws CardIssuerNotFoundException {

        LOG.info("Creating new card.");

        final Optional<CardIssuer> cardIssuer = this.cardIssuerService
            .getCardIssuerById(createCardDto.getCardIssuerId());

        if (cardIssuer.isEmpty()) {
            throw new CardIssuerNotFoundException(createCardDto.getCardIssuerId());
        }

        final Card card = CardAssembler.fromDto(createCardDto);
        card.setCardIssuer(cardIssuer.get());

        cardRepository.save(card);
    }

    /**
     * Gets the Card by identifier.
     *
     * @param id the Card identifier
     * @return the Card information
     * @throws CardNotFoundException if card is invalid
     */
    public CardDto getCardById(Long id) throws CardNotFoundException {

        LOG.info("Retrieving card with id: " + id + ".");

        CardDto cardDto;
        final Optional<Card> optionalCard = cardRepository.findById(id);
        if (optionalCard.isEmpty()) {
            throw new CardNotFoundException(id);
        } else {
            cardDto = CardAssembler.toDto(optionalCard.get());
        }
        return cardDto;
    }

    /**
     * Updates a Card by identifier.
     *
     * @param id            the Card identifier
     * @param updateCardDto an instance of UpdateCardDto
     * @throws CardNotFoundException if card is invalid
     */
    public void updateCard(Long id, UpdateCardDto updateCardDto) throws CardNotFoundException {

        final Optional<Card> optionalCard = cardRepository.findById(id);
        if (optionalCard.isEmpty()) {
            throw new CardNotFoundException(id);
        } else {
            Card card = optionalCard.get();
            card.setLastName(updateCardDto.getLastName());
            card.setPhoneNumber(updateCardDto.getPhoneNumber());
            card.setAddress(updateCardDto.getAddress());
            cardRepository.save(card);
        }
    }

    /**
     * Gets the Card by identifier.
     *
     * @param id the Card identifier
     */
    public void deleteCardById(Long id) {

        LOG.info("Deleting card with id: " + id + ".");

        cardRepository.deleteById(id);
    }
}
