package com.criticalsoftware.des.enterprisejava.blueprint.commons.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringDocConfig {

    /**
     * Basic SpringDoc configuration using Swagger_3 documentation type.
     *
     * @return the {@link OpenAPI} information
     */
    @Bean
    public OpenAPI api() {
        return new OpenAPI()
            .components(new Components())
            .info(new Info().title("Cards Application API"));
    }
}
