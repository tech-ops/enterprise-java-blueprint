package com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ResultErrorDto {

    private String errorCode;
    private String message;
    private List<ValidationError> errors;

    public ResultErrorDto() {
    }

    /**
     * Adds a new validation error.
     *
     * @param subError the validation error
     */
    public void addSubError(ValidationError subError) {
        if (this.errors == null) {
            this.errors = new ArrayList<>();
        }
        this.errors.add(subError);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<ValidationError> getErrors() {
        return errors;
    }
}
