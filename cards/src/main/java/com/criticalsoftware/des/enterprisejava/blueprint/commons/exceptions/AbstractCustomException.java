package com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCustomException extends Exception {

    private final List<Object> parameters;
    private final String resourceCode;
    private final String defaultMessage;

    AbstractCustomException(ResourceCode resourceCode) {
        this.resourceCode = resourceCode.getResourceCode();
        this.defaultMessage = resourceCode.getDefaultErrorMessage();
        this.parameters = new ArrayList<>();
    }

    public AbstractCustomException(ResourceCode errorCode, Long id) {
        this(errorCode);
        addParameter(id);
    }

    protected void addParameter(Object parameter) {
        this.parameters.add(parameter);
    }

    public List<Object> getParameters() {
        return this.parameters;
    }

    /**
     * Gets the exception error code.
     *
     * @return the {@link ResourceCode}
     */
    public ResourceCode getErrorCode() {
        return new ResourceCode() {
            private static final long serialVersionUID = 8862760722172549587L;

            public String getResourceCode() {
                return AbstractCustomException.this.resourceCode;
            }

            @Override
            public String getDefaultErrorMessage() {
                return AbstractCustomException.this.defaultMessage;
            }

        };
    }

    @Override
    public String getMessage() {
        return MessageFormat.format(this.defaultMessage, this.parameters.toArray());
    }
}
