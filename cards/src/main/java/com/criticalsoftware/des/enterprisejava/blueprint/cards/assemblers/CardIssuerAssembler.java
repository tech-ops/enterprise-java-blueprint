package com.criticalsoftware.des.enterprisejava.blueprint.cards.assemblers;

import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;

public class CardIssuerAssembler {

    /**
     * Transforms an instance of CardIssuer {@link CardIssuer} to an instance of CardIssuerDto {@link CardIssuerDto}.
     *
     * @param cardIssuer an instance of {@link CardIssuer}.
     * @return the {@link CardIssuerDto}
     */
    public static CardIssuerDto toDto(CardIssuer cardIssuer) {
        CardIssuerDto cardIssuerDto = null;

        if (cardIssuer != null) {
            cardIssuerDto = new CardIssuerDto(cardIssuer.getId(), cardIssuer.getName(), cardIssuer.getDescription());
        }

        return cardIssuerDto;
    }

}
