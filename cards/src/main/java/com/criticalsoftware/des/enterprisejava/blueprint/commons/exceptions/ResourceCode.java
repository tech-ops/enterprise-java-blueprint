package com.criticalsoftware.des.enterprisejava.blueprint.commons.exceptions;

import java.io.Serializable;

public interface ResourceCode extends Serializable {

    String getResourceCode();

    String getDefaultErrorMessage();

}
