package com.criticalsoftware.des.enterprisejava.blueprint.cards.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.UpdateCardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.services.CardIssuerService;
import org.springframework.stereotype.Component;

@Component
public class MutationResolver implements GraphQLMutationResolver {

    private CardIssuerService cardIssuerService;

    public MutationResolver(
        CardIssuerService cardIssuerService) {
        this.cardIssuerService = cardIssuerService;
    }

    /**
     * Updates an instance of {@link CardIssuer}.
     *
     * @param id                  the Card Issuer identifier
     * @param updateCardIssuerDto the information to be updated
     * @return information on the card issuer that was updated
     */
    public CardIssuerDto updateCardIssuer(Long id, UpdateCardIssuerDto updateCardIssuerDto) {

        cardIssuerService.updateCardIssuer(id, updateCardIssuerDto);

        return cardIssuerService.getCardIssuerDtoById(id);
    }
}
