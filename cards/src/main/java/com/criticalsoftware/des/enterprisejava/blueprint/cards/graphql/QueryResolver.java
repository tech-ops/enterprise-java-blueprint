package com.criticalsoftware.des.enterprisejava.blueprint.cards.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.dtos.CardIssuerDto;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.entities.CardIssuer;
import com.criticalsoftware.des.enterprisejava.blueprint.cards.services.CardIssuerService;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class QueryResolver implements GraphQLQueryResolver {

    private CardIssuerService cardIssuerService;

    public QueryResolver(CardIssuerService cardIssuerService) {
        this.cardIssuerService = cardIssuerService;
    }

    /**
     * Returns a list of CardIssuer {@link CardIssuer}.
     *
     * @return list of cardIssuers.
     */
    public List<CardIssuerDto> cardIssuers() {
        return cardIssuerService.getAllCardIssuers();
    }

}