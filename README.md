# Enterprise Java Blueprint

This project provides a set of best practices and guidelines that can be followed in order to develop a Java based application.
This documentation is organized by tool type as described in the following diagram. Apart from this, a set of project guidelines regarding several contexts are described in its own section.
The tools described are mere alternatives in a panoply of existent technologies.

Please refer to this presentation [Enterprise Java Blueprint Presentation](documentation/resources/enterprise-java-blueprint-presentation.pptx) and the following [Components Diagram](#components-diagram).

This project can also be used with the [Training Plan](./TRAINING_PLAN.MD).

<table>
<tr>
<td width="20%">

1. [Project Structure](documentation/PROJECT_STRUCTURE.MD)
2. [Application Framework](documentation/framework/APPLICATION_FRAMEWORK.MD)
3. [Build Tools](documentation/BUILD_TOOLS.MD)
4. [Web API Tools](documentation/webapitools/WEB_API_TOOLS.MD)
5. [Data Tools](documentation/DATA_TOOLS.MD)
6. [Quality Tools](documentation/qualitytools/QUALITY_TOOLS.MD)
7. [Logging](documentation/loggingtools/LOGGING_TOOLS.MD)
8. [Project Guidelines](documentation/guidelines/PROJECT_GUIDELINES.MD)

</td>
<td>

![Java Blueprint Diagram](documentation/resources/des_poc_diagram.jpg)

</td>
</tr>
</table>

<a name="components-diagram"></a>
## Components Diagram
The next diagram describes how these technologies relates with each other to structure a java based application.  

![Java Blueprint Overview](documentation/resources/enterprise-java-blueprint-overview.png)