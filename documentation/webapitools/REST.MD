## REST

![REST_logo](../resources/rest_logo.png)

This section provides information about REST architectural style and how to use it to build a java based web API;

* [Overview](#overview)
* [How to Configure](#how-to-configure)
* [How to Use](#how-to-use)
* [References](#references)

<a name="overview"></a>
### Overview

In this POC we use Spring's built in capabilities to build our RESTful API.
Spring offers a set of annotations to turn our controllers into a REST entry point, defining our endpoint methods along with those annotations.

<a name="how-to-configure"></a>
### How to configure

Add the following dependency to your project's pom.xml file:

```xml
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-web</artifactId>
	</dependency>
```

<a name="how-to-use"></a>
### How to Use

```java
@RestController
@RequestMapping("/api/cardissuers")
public class CardIssuerController {

    private CardIssuerService cardIssuerService;

    public CardIssuerController(final CardIssuerService cardIssuerService) {
        this.cardIssuerService = cardIssuerService;
    }

    @GetMapping("/")
    @Operation(summary = "Gets the list of all Card Issuers",
        responses = {
            @ApiResponse(responseCode = "200", description = "The list of Card Issuers",
                content = @Content(mediaType = "application/json"))})
    public ResponseEntity<List<CardIssuerDto>> getAllCardIssuers() {

        List<CardIssuerDto> cardIssuers = cardIssuerService.getAllCardIssuers();

        return new ResponseEntity<>(cardIssuers, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @Operation(summary = "Update an existing Card Issuer",
        responses = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Card Issuer not found",
                content = @Content(schema = @Schema(implementation = Void.class)))})
    public ResponseEntity<Void> updateCardIssuer(@PathVariable final Long id,
                                                 @RequestBody final UpdateCardIssuerDto editCardIssuer) {

        cardIssuerService.updateCardIssuer(id, editCardIssuer);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}


```
* As you can see in the above example, we use the annotation @RestController to inform our application that the methods implemented at our controller are our application's endpoints;
* Using the annotation @RequestMapping we map our web request to the annotated controller;
* Apart from that, using the annotations @GetMapping, @PostMapping, @PutMapping and @DeleteMapping we define the REST action that each method represents.

<a name="references"></a>
### References

* [Spring Web Services Documentation](https://docs.spring.io/spring-ws/docs/3.0.7.RELEASE/reference/)

[Go Back](WEB_API_TOOLS.MD)