## Web API
This section provides information about tools used to develop a Java based web API.
* [REST](REST.MD)
* [GraphQL](GRAPHQL.MD)
* [API Specification and Documentation](OPENAPI_SPECIFICATION_TOOLS.MD)

[Go Back](../../README.MD)