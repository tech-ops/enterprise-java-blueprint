## Checkstyle

![Checkstyle Logo](../resources/checkstyle-logo.png)

This section provides information about the tool used to check the java code for best practices.
* [Overview](#overview)
* [How to Configure](#how-to-configure)
* [How to Use](#how-to-use)
* [References](#references)

<a name="overview"></a>
### Overview

Checkstyle is a tool for checking Java source code for adherence to a Code Standard or set of validation rules (best practices),
by default we recommend the use of [Google Checkstyle configuration](https://github.com/checkstyle/checkstyle/blob/checkstyle-8.19/src/main/resources/google_checks.xml) and adjust as needed.

<a name="how-to-configure"></a>
### How to configure

>Find the sample file in *config/checkstyle/checkstyle.xml* with some custom adjustments:
* LineLength: 120
* Indentation
** basicOffset: 4
* JavadocMethod
** severity: error
** allowMissingParamTags: false
** allowMissingThrowsTags: false
** allowMissingReturnTag: false

In order to automate validations in your maven project please configure the [maven checkstyle plugin](https://maven.apache.org/plugins/maven-checkstyle-plugin/) in your project.

>The maven plugin and the checkstyle configurations must be compatible.

In order to configure the maven checkstyle plugin add the next configuration into your *pom.xml*.

```xml
<build>
  ...
  <pluginManagement>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>${maven.checkstyle.version}/version>
        <configuration>
          <configLocation>config/checkstyle/checkstyle.xml</configLocation>
          <encoding>UTF-8</encoding>
          <consoleOutput>true</consoleOutput>
          <failsOnError>true</failsOnError>
          <linkXRef>false</linkXRef>
        </configuration>
      </plugin>
    </plugins>
  </pluginManagement>
...
</build>
```

```xml
<build>
  ...
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-checkstyle-plugin</artifactId>
      <executions>
        <execution>
          <id>validate</id>
          <phase>validate</phase>
          <goals>
            <goal>check</goal>
          </goals>
        </execution>
      </executions>
    </plugin>
  </plugins>
  ...
</build>
```

<a name="how-to-use"></a>
### How to Use

<a name="references"></a>
### References

* [Maven checkstyle plugin](https://maven.apache.org/plugins/maven-checkstyle-plugin/)
* [Google Checkstyle configuration](https://github.com/checkstyle/checkstyle/blob/checkstyle-8.19/src/main/resources/google_checks.xml)

[Go Back](STATIC_ANALYSIS_TOOLS.MD)